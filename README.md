# My Weather App

**Aplicación del clima** es una app escrita en kotlin para el sistema operativo en Android, 
que permite cosultar el clima en ciudades de México.

Esta app trabaja con Android Studio y Kotlin en sus versiones más actuales.
