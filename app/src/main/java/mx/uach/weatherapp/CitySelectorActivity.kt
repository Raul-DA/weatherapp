package mx.uach.weatherapp

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.Toast

class CitySelectorActivity : AppCompatActivity() {

    //Identificador para pasar un parametro a la siguiente actividad
    val TAG = "CITY"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_city_selector)

        val btnCh = findViewById<Button>(R.id.btnCh)
        btnCh.setOnClickListener(View.OnClickListener {
            Toast.makeText(this, "Chihuahua", Toast.LENGTH_SHORT).show()
            val intent = Intent(this, MainActivity::class.java)
            intent.putExtra(TAG, "Chihuahua")
            startActivity(intent)
        })

        val btnMtry = findViewById<Button>(R.id.btnMtry)
        btnMtry.setOnClickListener(View.OnClickListener {
            Toast.makeText(this, "Monterrey", Toast.LENGTH_SHORT).show()
            val intent = Intent(this, MainActivity::class.java)
            intent.putExtra(TAG, "Monterrey")
            startActivity(intent)
        })
    }
}
